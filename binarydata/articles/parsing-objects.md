# Parsing Objects

You can read and write instances of custom classes or structs with the `ReadObject<T>`, `ReadObjects<T>` and `WriteObject<T>` methods. No further configuration is required to default to loading or saving the data held by public fields and properties. Through the use of `BinaryObjectAttribute` and `BinaryMemberAttribute`, this process can be controlled detailedly.

* The new serialization features originally planned for 5.x.x have been postponed for a later version, and the 5.x.x packages provide the same feature set as 4.x.x versions.
* Pre-4.x.x versions of the library [wrote members in a non-deterministic order](https://github.com/Syroot/BinaryData/issues/6). If you intend to use these features, please update to 4.x.x or newer and have a look at the [Ordering members](#ordering-members) section on how to provide a deterministic order.

## Default behavior

By default, data is read and written in the following manner:
* Only types with a default, parameterless constructor (of any accessibility) can be read.
* Members are written in alphabetical (ordinal-sorted) order.
* Inherited members are ignored.
* Only public fields, properties with both public getter and setter, or members explicitly decorated with a `BinaryMemberAttribute` are considered.
* `Boolean` values are stored with `BooleanCoding.Byte` (the .NET default).
* `DateTime` values are stored with `DateTimeCoding.NetTicks` (the library default).
* `Enum` values are not strictly checked for being defined or being a valid set of flags for `FlagsAttribute` decorated enums.
* `String` values are stored with `StringCoding.VariableByteCount` (the .NET default).
* There is no padding or any spacing between members, values are continuously read from the most recent stream position.

By decorating your class / struct with the `BinaryObjectAttribute` or members with the `BinaryMemberAttribute`, you can modify this behavior to fit your needs.

## Inherited values

Decorate a class with `[BinaryObject(Inherit = true)]` to also consider inherited values. Base type values are read or written first before handling the data of the specialized class.

This process is recursive until the first class is not decorated with the attribute and `Inherit = true` anymore.

```cs
public class BaseTest
{
	public int HandledFirst;
}

[BinaryObject(Inherit = true)]
public class TestClass : BaseTest
{
	public int HandledSecond;
}
```

## Ignore public values

If you only want to consider members explicitly decorated with the `BinaryMemberAttribute`, you can prevent reading and writing data stored in public fields and properties by decorating your class with `[BinaryObject(Explicit = true)]`.

```cs
[BinaryObject(Explicit = true)]
public class TestClass
{
	[BinaryMember]
	public int WillHandle;

	public int WillIgnore;
}
```

## Ordering members

By default, all members are read and written in alphabetical, ordinal order. To specify a custom order, provide the `Order` property with a unique value (positive or negative). All members missing this property are written afterwards in alphabetical, ordinal order.

```cs
[BinaryObject(Explicit = true)]
public class TestClass
{
        [BinaryMember(Order = -1)]
	public int SerializedFirst;

	[BinaryMember(Order = 0)]
	public int SerializedSecond;

	[BinaryMember(Order = 2)]
	public int SerializedFourth;

	public int SerializedFifth;

	[BinaryMember(Order = 1)]
	public int SerializedThird;
}
```

## Handling different `Boolean`, `DateTime` and `String` formats

You can decorate `Boolean`, `DateTime` and `String` members with a `BinaryMemberAttribute` to specifiy the `BooleanCoding`, `DateTimeCoding` and `StringCoding` to use.

Note that when using `StringCoding.Raw` on strings, you also have to specify the `Length` property.

Formats not valid for the member type are ignored (e.g., specifying a `BooleanCoding` on an `Int32` member).

```cs
public class TestClass
{
	[BinaryMember(BooleanCoding = BooleanCoding.NonZeroDword)]
	public bool W;

	[BinaryMember(DateTimeCoding = DateTimeCoding.CTime)]
	public DateTime X;

	[BinaryMember(StringCoding = StringCoding.ZeroTerminated)]
	public String Y;

	[BinaryMember(StringCoding = StringCoding.Raw, Length = 5)]
	public String Z;
}
```

## Strictly checking `Enum` values

Enum values can be validated before handling them, as described on the [Enum Values](https://github.com/Syroot/BinaryData/wiki/Enum-Values) page. This validation can be controlled with the `Strict` parameter of the `BinaryMemberAttribute`:
```cs
public class TestClass
{
	[BinaryMember(Strict = true)]
	public TestEnum Checked;

	public TestEnum NotChecked;
}

enum TestEnum
{
	SomeValidValue = 12
}
```

## Handling arrays

Arrays cannot be read unless decorated with a `BinaryMemberAttribute` which provides a `Length` value, determining the number of elements to read.

When writing such array, the length of the array is *not* checked. This is because the length of some `IEnumerable` instances cannot be retrieved beforehand.

```cs
public class TestClass
{
	[BinaryMember(Length = 5)]
	public int[] FiveNumbers;
}
```

## Modifying stream position between members

If you want to ignore or introduce padding between members or read and write unions, you can modify the current position in the stream before the member's value is handled. By default, the next value is handled at the current stream position.

Decorate the affected members with the `BinaryMemberAttribute` and specify one or all of the following:

* `Offset`: The number of bytes to move in the stream before handling the value. Can be negative. Defaults to 0 to stay at the current position.
* `OffsetOrigin`: Configures the origin of the offset (s. above). `Begin` is relative to the start address where the first (inherited) value was handled. `Current` is relative to the current stream position. Defaults to the `Current` stream position to stay at the current position when `Offset` defaults to 0 aswell.

```cs
public class TestClass
{
	[BinaryMember(Offset = 0, OffsetOrigin = OffsetOrigin.Begin)]
	public int UnionX;

	[BinaryMember(Offset = 0, OffsetOrigin = OffsetOrigin.Begin)]
	public int UnionY;

	[BinaryMember(Offset = 4)]
	public int PaddedBy4Bytes;
}
```

## Using custom value converters

If the existing binary formats are not sufficient, it is possible to implement `IBinaryConverter` by a custom class, which type is then passed to the `Converter` property of `BinaryMemberAttribute`.

* When an affected member is read, the converter's `Read` method is invoked with the `Stream`, the currently handled object instance and the `BinaryMemberAttribute` instance the member was decorated with. The method is expected to _return_, not set the parsed value (the latter is not prevented, but will be overwritten by the returned value).
* When an affected member is written, the converter's `Write` method is invoked in the same fashion. The value to convert is passed to the method - it can also be retrieved from the passed object instance manually if this is preferred.

The `Stream` is already offset as possibly specified with the `OffsetOrigin` and `Offset` properties of the `BinaryMemberAttribute`.

The passed object instance is mostly useful to validate if the member should be handled at all (in case another property which has already been handled controls this).

```cs
public class TestClass
{
	[BinaryMember(Converter = typeof(DuplicateConverter)]
	public int UnionX;
}

public class DuplicateConverter : IBinaryConverter
{
	public object Read(Stream stream, object instance, BinaryMemberAttribute memberAttribute, ByteConverter converter)
	{
		return stream.ReadInt32(converter: converter) * 2;
	}

	public void Write(Stream stream, object instance, BinaryMemberAttribute memberAttribute, object value, ByteConverter converter)
	{
		int intValue = (int)value;
		stream.Write(value / 2, converter: converter);
	}
}
```

## Performance and limitations

Handling object structures requires caching metadata about the type the first time it is handled. The cache exists throughout a program's lifetime. The process of acquiring the object metadata is handled by sanely optimized reflection code and should not be a bottleneck. However, if you read large arrays of objects (500 or more instances), the slight overhead can become costly, and hand-crafted methods are preferrable then.

The limitations in implementing attributes and their properties (and keeping those manageable) introduces several restrictions which simply cannot be solved without custom loading and saving code. A RIFF file, for example, should not (and most possibly cannot) be handled with this method; but repetitive structures or chunks of data without a lot of conditions are great candidates for these features.

While it is possible to read any object with the `ReadObject<T>` and `ReadObjects<T>` methods, including built-in types like `Boolean` or `Int32`, it is recommended to use the corresponding `ReadXxx` methods for this:
* `ReadObject` does type-switching on the passed parameter to eventually just invoke the corresponding `ReadXxx` methods for built-in types, which could have been invoked directly beforehand.
* Additional configuration like data formats cannot be passed to the `ReadObject` methods.
