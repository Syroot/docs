# Parsing Strings

Multiple binary string representations other than the .NET default are supported:

| `StringCoding`      | `Span` method | Description |
| ------------------- | ------------- | ----------- |
| `VariableByteCount` | `String`      | The text data is prefixed with a 7-bit encoded `Int32`, specifying the number of characters. This is the .NET default.
| `ByteCharCount`     | `String1`     | The text data is prefixed with a single byte, specifying the number of characters.
| `Int16CharCount`    | `String2`     | The text data is prefixed with a signed two-byte value, specifying the number of characters.
| `Int32CharCount`    | `String4`     | The text data is prefixed with a signed four-byte, specifying the number of characters.
| `ZeroTerminated`    | `String0`     | The text data is 0-terminated, thus not prefixed and read until a `0` is read.**
| `Raw`*              | `StringRaw`   | The text data has no prefix or postfix and the length must be provided when reading.
| N/A                 | `StringFix`   | Same as `Raw`, but always fitting into the given number of bytes (trimming trailing 0 bytes when reading data, and padding with 0 bytes when writing data).

\* Can only be used for writing data; for reading such data, use the `ReadString` overloads accepting the fixed string length instead.<br>
\** The number of bytes required for this 0 value depends on the encoding (for example, UTF16 requires 2 bytes, while UTF8 requires only one).

## Encoding

The encoding to use for the text data can be specified in the following ways:
- `Stream` extensions: Pass as a parameter.
- `BinaryStream`: Pass in the constructor or set as the `Encoding` property any time.
- `SpanReader/Writer`: Pass in the constructor or set as the `Encoding` property any time.

If not specified, it defaults to UTF8 encoding.

## Byte representation

Writing the string "Hello" with ASCII encoding and little endian byte order yields the following bytes. The raw ASCII bytes are bolded.

| `Span` method call           |   00   |   01   |   02   |   03   |   04   |   05   |   06   |   07   |   08   |
| ---------------------------- | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| `WriteString("Hello")`       |   05   | **48** | **65** | **6C** | **6C** | **6F**
| `WriteString1("Hello")`      |   05   | **48** | **65** | **6C** | **6C** | **6F**
| `WriteString2("Hello")`      |   05   |   00   | **48** | **65** | **6C** | **6C** | **6F**
| `WriteString4("Hello")`      |   05   |   00   |   00   |   00   | **48** | **65** | **6C** | **6C** | **6F**
| `WriteString0("Hello")`      | **48** | **65** | **6C** | **6C** | **6F** |   00
| `WriteStringRaw("Hello")`    | **48** | **65** | **6C** | **6C** | **6F**
| `WriteStringFix("Hello", 7)` | **48** | **65** | **6C** | **6C** | **6F** |   00   |   00

## Code sample

In the following sample, the `Stream` extension methods are used to write strings in specific formats:

```cs
using (MemoryStream stream = new MemoryStream())
{
	string magicBytes = stream.ReadString(4);
	if (magicBytes != "RIFF")
		throw new InvalidOperationException("Not a RIFF file.");

	string zeroTerminated = stream.ReadString(StringCoding.ZeroTerminated);
	string unicodeString = stream.ReadString(StringCoding.Int32CharCount, Encoding.UTF8);
	string netString = stream.ReadString();
	string anotherNetString = stream.ReadString(StringCoding.Int32CharCount);

	stream.Write("RAW", StringCoding.Raw);
}
```
