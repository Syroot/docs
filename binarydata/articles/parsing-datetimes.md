# Parsing DateTimes

A date and timestamp can be stored in different binary representations. The following are supported by the library:

| `DateTimeCoding` | `Span` methods | Description |
| ---------------- | -------------- | ----------- |
| `NetTicks`       | `DateTime`     | Represents the time as ticks as stored in the [`DateTime.Ticks`](https://msdn.microsoft.com/en-us/library/system.datetime.ticks.aspx) property. This is the library and .NET default.
| `CTime`          | `DateTimeT`    | Represents an unsigned 32-bit [`time_t`](http://en.cppreference.com/w/c/chrono/time_t) from the C standard library, storing the seconds since 1970-01-01 until approximately 2106-02-07.
| `CTime64`        | `DateTimeT64`  | Represents an unsigned 64-bit [`time_t`](http://en.cppreference.com/w/c/chrono/time_t) from the C standard library, storing the seconds since 1970-01-01 until approximately 292277026596-12-04.
