# Handling Endianness

Little endian as well as big endian is supported when reading and writing multi-byte data. You can force a specific byte order for qualifying methods in the following ways:
- `Stream` extensions: Pass one of the static `ByteConverter` instances.
- `BinaryStream`: Pass one of the static `ByteConverter` instance to the constructor or set as the `ByteConverter` property any time.
- `SpanReader/Writer`: Pass one of the `Endian` enum values to the constructor or set as the `Endian` property any time.

If no byte order is specified, it defaults to the system byte order. You can detect the system byte order in several ways:

| Call                                                 | LE system       | BE system       |
| ---------------------------------------------------- | --------------- | --------------- |
| `System.BitConverter.IsLittleEndian`                 | `true`          | `false`         |
| `Syroot.BinaryData.Core.EndianTools.SystemEndian`    | `Endian.Little` | `Endian.Big`    |
| `Syroot.BinaryData.Core.EndianTools.NonSystemEndian` | `Endian.Big`    | `Endian.Little` |
| `Syroot.BinaryData.ByteConverter.System.Endian`      | `Endian.Little` | `Endian.Big`    |

## Code sample

The following code demonstrates using the `Stream` extension methods to specifiy a one-time byte order change:

```cs
stream.Write(0x12345678, ByteConverter.Big);
stream.Write(0x87654321, ByteConverter.Little);
```

The `Syroot.BinaryData.ByteConverter` instances offer functionality similar to .NET's `BitConverter` singleton; you can use them for your own purposes aswell:
```cs
byte[] buffer = new byte[sizeof(int) * 2];
ByteConverter.Big.GetBytes(133766642, buffer);
// Write the next value starting at byte index 4.
ByteConverter.Little.GetBytes(133766642, buffer, sizeof(int));
// Retrieve the Int32 value from the raw bytes again.
int secondValue = ByteConverter.Little.ToInt32(buffer, 4);
```
