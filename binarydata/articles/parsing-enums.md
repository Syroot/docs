# Parsing Enums

Enum values can be read or written in a strongly-typed manner. The size of each value is taken from the underlying type (which defaults to `int` if not explicitly defined in the enum declaration).

Optinally, you can validate the read or written values to only allow values actually defined in the enum, or, for enums decorated with the `FlagsAttribute`, valid bit flags of the available values:

| `strict` parameter | `Span` method   | Description                    |
| ------------------ | --------------- | ------------------------------ |
| `false`            | `WriteEnum`     | Any value is accepted.         |
| `true`             | `WriteEnumSafe` | The parsed value is validated. |
