# Parsing Booleans

Typically, .NET stores a boolean value in 1 byte representing either `1` for true or `0` for false. Some file formats store a boolean in larger data types, which can be read or written with the following:

| `BooleanCoding` | `Span` methods | Description |
| --------------- | -------------- | ----------- |
| `Byte`          | `Boolean`      | The boolean is represented by 1 byte with `0` representing `false` and anything else being `true` (being `1` when written). This is the .NET default.
| `Word`          | `Boolean2`     | The boolean is represented by 2 bytes with `0` representing `false` and anything else being `true` (being `1` when written).
| `Dword`         | `Boolean4`     | The boolean is represented by 4 bytes with `0` representing `false` and anything else being `true` (being `1` when written).

Note that byte order only matters when _writing_ values (non-zero data evaluating to `true` is non-zero no matter the endianness), thus there are no `Read*` overloads accepting a `ByteConverter` instance.

## Byte representation

Writing the `true` with little endian byte order yields the following bytes:

| `Span` method call    | 00 | 01 | 02 | 03 |
| --------------------- | -- | -- | ---| -- |
| `WriteBoolean(true)`  | 01
| `WriteBoolean2(true)` | 01 | 00
| `WriteBoolean4(true)` | 01 | 00 | 00 | 00
