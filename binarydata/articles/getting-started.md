# Getting Started

## Requirements

The libraries require .NET Standard 2.0 - the minimal required version follows Microsoft's support lifecycle.

## NuGet packages

You can easily add the functionality provided by the libraries by installing the following NuGet packages, of which
each provides different ways to access data:

### [Syroot.BinaryData](https://www.nuget.org/packages/Syroot.BinaryData)

Use this package if you want to extend functionality provided by `Stream` instances:
- `Stream` extensions: Include the `Syroot.BinaryData` namespace, and any class inheriting from `Stream` provides
  many new methods for reading or writing specific data formats.
- `BinaryStream` class: You can create a `BinaryStream` instance from an existing `Stream`, and when operating on the
  stream through it, it uses the data formats you prefer (like how many bytes you want to use for a boolean, how the
  length of strings is encoded, and which byte order to use).

### [Syroot.BinaryData.Memory](https://www.nuget.org/packages/Syroot.BinaryData.Memory)

Use this package if you want to quickly parse data available in `(ReadOnly)Span<byte>` instances:
- `SpanReader`/`SpanWriter`: Works on .NET's new `(ReadOnly)Span<byte>` instances for high performance parsing of
  specific data formats, with familiar functionality similar to a stream by pointing to a virtual position in them.

### [Syroot.BinaryData.Serialization](https://www.nuget.org/packages/Syroot.BinaryData.Serialization)

Use this package if you want to quickly read and write instances of any class to a `Stream`.
- `Stream` extensions: Adds object serialization methods to automatically parse class structures.

## Building from source

The source code of the library is available on [GitLab](https://gitlab.com/Syroot/BinaryData).

## Deprecated 4.x and signed packages

The previous `Syroot.IO.BinaryData` package has been split into the core and the serialization packages mentioned
above and is now deprecated. The old packages have been unlisted as of May 2019. Beyond many new features, several breaking
changes were made. Please have a look at the most recent [release notes](https://gitlab.com/Syroot/BinaryData/tags) for
more info.

Signed packages have also been unlisted as this legacy scenario is no longer supported by new versions.

Any unlisted packages can still be installed if referenced directly in a project.
