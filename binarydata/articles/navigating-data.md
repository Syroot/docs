# Navigating Data

Additional methods simplify navigating in binary files, especially those read or written in non-sequential ways.

## Block alignment

To align the reader or writer to the next multiple of a given block size (filling the data in-between with 0 in case of the writer), call `Align(int alignment)` and the reader seeks to the start of the next block.

```cs
using (MemoryStream stream = new MemoryStream())
{
	string header = stream.ReadString(4);
	Debug.Assert(header == "TEST");
	// Seek to the next block.
	stream.Align(0x200);
}
```

Pass a negative alignment to align to the previous complete block.

## Temporary seeking

When skipping to another section in the stream to read data from there (or write data to), but then wanting to continue reading / writing from the position the stream had before the skip, `TemporarySeek` can be used together with the `using` pattern.

```cs
using (MemoryStream stream = new MemoryStream())
{
	int offset = stream.ReadInt32();
	using (stream.TemporarySeek(offset, SeekOrigin.Begin))
	{
		byte[] dataAtOffset = stream.ReadBytes(128);
	}
	int dataAfterOffsetValue = stream.ReadInt32();
}
```

Internally, this returns a disposable `Seek` instance which holds information about the data offset and the address to return to. It can also be used manually, though this is syntactically not recommended.

## Moving (simulated seeks)

For non-seekable streams, it is sometimes required to skip a few bytes. You can simulate a `Seek` by using the `Move` method, which just discards read bytes (in case of readable `Stream` instance) or writes 0-bytes (in case of writable `Stream` instance).
