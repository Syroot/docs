# NintenTools

Please select a topic from the navigation on the left or a following link for more information:

## BFRES

- [GitLab repository](https://gitlab.com/Syroot/NintenTools.Bfres)
- [NuGet package](https://www.nuget.org/packages/Syroot.NintenTools.Bfres)

## BYAML

- [GitLab repository](https://gitlab.com/Syroot/NintenTools.Byaml)
- [NuGet package](https://www.nuget.org/packages/Syroot.NintenTools.Byaml)

## Mario Kart 8

- [GitLab repository](https://gitlab.com/Syroot/NintenTools.MarioKart8)
- [NuGet package](https://www.nuget.org/packages/Syroot.NintenTools.MarioKart8)

## Yaz0

- [GitLab repository](https://gitlab.com/Syroot/NintenTools.Yaz0)
- [NuGet package](https://www.nuget.org/packages/Syroot.NintenTools.Yaz0)
