# CliTools

This library provides helper classes to make working with the command line interface easier.

A quick feature tour is given in the following articles. For the full API documentation, consult the reference from the navigation bar on the left.
