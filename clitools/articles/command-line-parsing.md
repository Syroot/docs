# Command Line Parsing

With the `CommandLine` class, you can parse command line arguments into a strongly-typed class instance without writing any code, only by defining properties and fields.

To get started, create a new class inheriting from the abstract `CommandLine` class and add members to parse as arguments.

## Argument Members

Simply define members in the class to fill in from the command line arguments. The members need to meet the following requirements:
- They must not be static.
- Fields must not be `readonly` or `private`.
- Properties must not have a private `get` or `set` method.
- `String` members are assigned directly with a single pair of quotes at the start and end stripped if found.
- `Byte`, `Decimal`, `Double`, `Int16`, `Int32`, `Int64`, `SByte`, `Single`, `UInt16`, `UInt32` and `UInt64` members are parsed with the current user culture before they are assigned. If the value could not be parsed, the argument is ignored.
- `Boolean` members simply become `true` if the name is found in the command line (s. below).

### Named arguments

The following command line includes a program name and passes only named arguments for whose the order does not matter:

`FancyProg.exe port 23456 /NAME MyServer /useUdp -welcomeMsg "Welcome on the server!"`

Note the following:
- The names of arguments must be prefixed with `-` or `/`.
- The names are case-insensitive.
- String values with spaces require quotes at the start and end.
- Boolean values do not have any value following.

These can be fetched by adding members of the argument names to the class like this:
```cs
class MyCommandLine : CommandLine
{
    // Will be 23456
    internal ushort Port;

    // Will be true
    internal bool UseUdp;

    // Will be "MyServer"
    public string Name { get; set; }

    // Will be "Welcome on the server!" (quotes are stripped).
    // You can remap specific names, required if obfuscation garbles names.
    [CommandLineArg(Name = "WelcomeMsg")]
    internal string WelcomeMessage { get; set; }
}
```

### Positional arguments

The following command line includes positional arguments at the start:

`FancyProg.exe 23456 MyServer /useUdp "Welcome on the server!"`

Note the following:
- There are no names prefixing the first two and the last value ("/useUdp" is a named boolean argument).
- The order of those values matters.
- If named arguments appear in-between (those prefixed with `/` or `-`), indexing continues with the next indexed argument.
- `Boolean` members are not supported as positional arguments.

These can be fetched with the following members which require the `CommandLineArgAttribute` to provide their index. The index always starts with 1, as 0 is already handled as the `ProgramName` property (even if it is not available).
```cs
class MyCommandLine : CommandLine
{
    // Will be 23456
    [CommandLineArg(1)]
    internal ushort Port;

    // Will be true
    internal bool UseUdp;

    // Will be "MyServer"
    [CommandLineArg(2)]
    public string Name { get; set; }

    // Will be "Welcome on the server!" (quotes are stripped).
    [CommandLineArg(3)]
    internal string WelcomeMsg { get; set; }
}
```

### Required arguments

You can set any member to be required, which will throw a `CommandLineException` if its corresponding argument is missing.
```cs
class MyCommandLine : CommandLine
{
    [CommandLineArg(1, Required = true)]
    internal ushort Port;

    [CommandLineArg(Required = true)]
    internal bool UseUdp;
}
```

### Automated help messages

From the metadata collected from the members and attributes, you can automatically build a help message informing the user about how to invoke your application.

You typically want to provide descriptions for each member which you can do with the `CommandLineArg.Description` parameter.
```cs
class MyCommandLine : CommandLine
{
    [CommandLineArg(Description = "Sets the server port under which it can be reached.")]
    internal ushort Port;

    [CommandLineArg(Description = "If provided, UDP communication is done rather than TCP.")]
    internal bool UseUdp;
}
```

Call `CommandLine.GetHelpString("This starts a cool server app.")` to receive the following string.
```
This starts a cool server app.

MYAPP  [/Port value]  [/UseUdp]

	Port         Sets the server port under which it can be reached.
	UseUdp       If provided, UDP communication is done rather than TCP.
```
Note that the introductionary title parameter is optional. The program name displayed in uppercase is retrieved at runtime to match possibly renamed executable files.

## Filling the members from the command line

### Default command line

To fill out your properties now, call the `Parse` method with no parameters. This will fill out the properties of the instance with the values parsed from `Environment.GetCommandLineArgs()`.
```cs
MyCommandLine commandLine = new MyCommandLine();
commandLine.Parse();
```

### Arbitrary command lines

You can call the `Parse(IList<string> args, bool hasProgramNameArg)` constructor to pass arbitrary arrays to be parsed as a command line.

Since some command lines include the executable program name as the first argument at index 0 and some others don't include it (the element at index 0 then already contains the first parameter), the `hasProgramNameArg` has to be set accordingly.

```cs
class MyCommandLine : CommandLine
{
    // To parse args which contain the program name as first element, e.g.
    // args[0] = "C:\Program Files (x86)\Crazy Company\Fancy Prog\Fancy Prog.exe"
    // args[1] = "23456"
    // args[2] = "/useUdp"
    // args[3] = "/name"
    // args[4] = "Funny Server"
    MyCommandLine(IList<string> args) : base(args, true) { }

    // To parse args which do not contain the program name as first element, e.g.
    // args[0] = "23456"
    // args[1] = "/useUdp"
    // args[2] = "/name"
    // args[3] = "Funny Server"
    MyCommandLine(IList<string> args) : base(args, false) { }
}
```

The program name (and path) and can be accessed via the `string CommandLine.ProgramName` property. If it is not available in the arguments passed to the constructor (with the boolean parameter being `false` accordingly), it will return `null`.
