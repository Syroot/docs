# Getting Started

## Requirements

The libraries require .NET Standard 2.0 - the minimal required version follows Microsoft's support lifecycle.

## NuGet packages

The library is available in the [Syroot.CliTools](https://www.nuget.org/packages/Syroot.CliTools) NuGet package.

## Building from source

The source code of the library is available on [GitLab](https://gitlab.com/Syroot/CliTools).
